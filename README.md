# Pinewood Derby #

This project contains files for creating a pinewood derby timer.

## Hardware ##
* Teensy LC board (with breadboard)
* 1 button (with 3D printed handle)
* 4 break-beam sensors

## Setup ##
* Setup track, including setting up the breakbeam sensors at the finish line
* Make sure the button and all sensors are plugged into the breadboard in the correct locations
* Start computer/laptop
* Start a text application that will receive the info from the timing system (i.e. Notepad, Word, Excel, etc.)
* Plug the USB cable into the Teensy LC board
* Plug the USB cable into the computer/laptop
* Make sure the computer/laptop is focused on the text application - you can type on the keyboard to verify the cursor is in the right spot and ready, then delete that text

At this point, everything should be up and running. Perform a test run by pressing the start button and then breaking all four of the beams at the finish line (with your hand, cars, or any other object). It should report four times after the last bean has been broken. The timer automatically resets after reporting the times.

## Use ##
* Make sure the computer/laptop is focused on the text application - you can type on the keyboard to verify the cursor is in the right spot and ready, then delete that text
* Lower the start gate on the track while simultaneously pressing the start button on the timer
* When the cars break the beams at the finish line, the times are stored
* When the last car finishes the race, all times are sent to the computer/laptop as if they were typed by a keyboard
* The timer automatically resets and prepares itself for the next race - take care not to press the start button at this point in time as there is no indication that the timer believes the race has started
 * When in doubt, pass your hand through each/all of the beams to ensure the timer has reset and is ready