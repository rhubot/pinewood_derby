/*********************************************************************
 *
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2017, David Rusbarsky
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the author nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 * Author: David Rusbarsky
 *********************************************************************/

/**
 * This code works as a timer for a pinewood derby race.
 * It takes in a button press as the start of the race and then
 * monitors 4 break-beam sensors. Once all four sensors have been
 * triggered, the times (between the button press and when the beam
 * was broken) for each lane is reported as if they were typed out by
 * a keyboard.
 * 
 * Note: A computer is needed since the microcontroller is acting like
 * a keyboard device. Likewise, it is important to have the computer
 * focused on the application that will recieve the key strokes (i.e.
 * notepad, Excel, or similar application) throughout each race.
 * 
 * The output will come in the form of "Lane X: N seconds"
 * Where X is the lane number (1-4)
 *       N is seconds
 */

// Pins
const int BUTTON_PIN = 8;
const int LANE_1_PIN = 5;
const int LANE_2_PIN = 16;
const int LANE_3_PIN = 17;
const int LANE_4_PIN = 21;

// Variables related to the state of the race
const int RACE_STATE_WAITING = 0;
const int RACE_STATE_STARTED = 1;
const int RACE_STATE_FINISHED = 2;
int raceState = RACE_STATE_WAITING;

// Variables related to race time
unsigned long startTime = 0;
unsigned long lane1EndTime = 0;
unsigned long lane2EndTime = 0;
unsigned long lane3EndTime = 0;
unsigned long lane4EndTime = 0;

unsigned long minTriggerTime = 3;
unsigned long lane1TriggerTime = 0;
unsigned long lane2TriggerTime = 0;
unsigned long lane3TriggerTime = 0;
unsigned long lane4TriggerTime = 0;

/**
 * Record the start time and change the state variable to "started"
 */
void startRace() {
  startTime = millis();
  raceState = RACE_STATE_STARTED;
  Keyboard.println("Ready? Set? Go!");
}

/**
 * When each car crosses the finish line, call this function to record the time
 */
void finishedRace(int lane) {
  switch(lane){
    case 1:
      //Keyboard.println("Lane 1 triggered");
      if(0 == lane1TriggerTime){
        lane1TriggerTime = millis();
      } else if( (0 == lane1EndTime) && ((millis() - lane1TriggerTime) > minTriggerTime) ) {
        lane1EndTime = millis();
      }
      break;
    case 2:
      if(0 == lane2TriggerTime){
        lane2TriggerTime = millis();
      } else if( (0 == lane2EndTime) && ((millis() - lane2TriggerTime) > minTriggerTime) ) {
        lane2EndTime = millis();
      }
      break;
    case 3:
      if(0 == lane3TriggerTime){
        lane3TriggerTime = millis();
      } else if( (0 == lane3EndTime) && ((millis() - lane3TriggerTime) > minTriggerTime) ) {
        lane3EndTime = millis();
      }
      break;
    case 4:
      if(0 == lane4TriggerTime){
        lane4TriggerTime = millis();
      } else if( (0 == lane4EndTime) && ((millis() - lane4TriggerTime) > minTriggerTime) ) {
        lane4EndTime = millis();
      }
      break;
    default:
      String errMessagePartA = String("An invalid lane (");
      String errMessagePartB = String(") reported finishing the race. I'll just ignore it for now...");
      String errMessage = String(errMessagePartA + lane + errMessagePartB);
      Keyboard.println("SOMETHING WENT WRONG!");
      delay(50);
      Keyboard.println(errMessage);
      delay(50);
      break;
  }

  // Check to see if all lanes have finished...
  if( (0 != lane1EndTime) &&
      (0 != lane2EndTime) &&
      (0 != lane3EndTime) &&
      (0 != lane4EndTime) ) {
    raceState = RACE_STATE_FINISHED;
  }
}

/**
 * Outputs the times for each of the lanes as if they were typed out by a keyboard.
 * 
 * The output will come in the form of "Lane X: N seconds"
 * Where X is the lane number (1-4)
 *       N is seconds
 */
void reportTimes() {
  unsigned long timeDiff;
  double seconds;
  String messagePartA;
  String messagePartC;
  String message;

  // Lane 1
  timeDiff = lane1EndTime - startTime;
  seconds = timeDiff / 1000.0;
  messagePartA = String("Lane 1: ");
  messagePartC = String(" seconds");
  message = messagePartA + seconds + messagePartC;
  Keyboard.println(message);
  delay(50);
  
  // Lane 2
  timeDiff = lane2EndTime - startTime;
  seconds = timeDiff / 1000.0;
  messagePartA = String("Lane 2: ");
  messagePartC = String(" seconds");
  message = messagePartA + seconds + messagePartC;
  Keyboard.println(message);
  delay(50);
  
  // Lane 3
  timeDiff = lane3EndTime - startTime;
  seconds = timeDiff / 1000.0;
  messagePartA = String("Lane 3: ");
  messagePartC = String(" seconds");
  message = messagePartA + seconds + messagePartC;
  Keyboard.println(message);
  delay(50);
  
  // Lane 4
  timeDiff = lane4EndTime - startTime;
  seconds = timeDiff / 1000.0;
  messagePartA = String("Lane 4: ");
  messagePartC = String(" seconds");
  message = messagePartA + seconds + messagePartC;
  Keyboard.println(message);
  delay(50);
}

/**
 * Reset the variables to prepare for a new race
 */
void resetRace() {
  // Reset the time variables
  startTime = 0;
  lane1EndTime = 0;
  lane2EndTime = 0;
  lane3EndTime = 0;
  lane4EndTime = 0;
  
  // Reset the race state in preparation for the next race
  raceState = RACE_STATE_WAITING;
}

/**
 * Get ready to run the application
 */
void setup() {
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  pinMode(LANE_1_PIN, INPUT_PULLUP);
  pinMode(LANE_2_PIN, INPUT_PULLUP);
  pinMode(LANE_3_PIN, INPUT_PULLUP);
  pinMode(LANE_4_PIN, INPUT_PULLUP);
}

/**
 * Logic loop automatically called (by Arduino) at a regular rate
 */
void loop() {
  int reading;
  int lane1Reading;
  int lane2Reading;
  int lane3Reading;
  int lane4Reading;
  
  // Determine the state of the race (Waiting, Started, Finished)
  switch(raceState) {
    
    case RACE_STATE_WAITING:
      // See if the start button has been pressed yet
      reading = digitalRead(BUTTON_PIN);

      // The button goes low when pressed because of the pull-up resistor
      if(LOW == reading) {
        startRace();
      }
      break;
      
    case RACE_STATE_STARTED:
      // Check the status of each of the break-beam sensors
      lane1Reading = digitalRead(LANE_1_PIN);
      lane2Reading = digitalRead(LANE_2_PIN);
      lane3Reading = digitalRead(LANE_3_PIN);
      lane4Reading = digitalRead(LANE_4_PIN);

      // The sensor goes low when the beam is broken
      if(LOW == lane1Reading) {
        finishedRace(1);
      } else {
        lane1TriggerTime = 0;
      }
      if(LOW == lane2Reading) {
        finishedRace(2);
      } else {
        lane2TriggerTime = 0;
      }
      if(LOW == lane3Reading) {
        finishedRace(3);
      } else {
        lane3TriggerTime = 0;
      }
      if(LOW == lane4Reading) {
        finishedRace(4);
      } else {
        lane4TriggerTime = 0;
      }
      break;
      
    case RACE_STATE_FINISHED:
      reportTimes();
      resetRace();
      break;
      
    default:
      Keyboard.println("SOMETHING WENT WRONG!");
      delay(50);
      Keyboard.println("I am resetting myself and we will need to run that race again - Sorry!");
      delay(50);
      
      resetRace();
      break;
  }
}
